\documentclass{acm_proc_article-sp}

\usepackage{float}
\usepackage{graphicx}

\begin{document}

\title{Aberystwyth University \\ CS21120\\
Assignment 2 - Scheduling}

\numberofauthors{1}

\author{
\alignauthor
	agilob\\
	agilob [a-t] gmail com
}

\date{28-04-2013}

\maketitle
\begin{abstract}
The paper provides examples of some simple scheduling algorithms with descriptions, analysis and benchmarks in Java programming language as well as presents methodology, results and gives conclusions about them. \\
The following scheduling algorithms are described in this document:
\begin{itemize}
  \item Round robin
  \item Shortest job first
  \item First in first served
  \item Shortest remaining time
  \item Priority based scheduling
\end{itemize}
Also, each of the algorithms was tested with four provided test files.
\end{abstract}


\section{Introduction}

\textbf{Scheduling algorithm} - is a kind of algorithm that solves one of the most important problems in IT - how to share computational power between processes to provide most possible efficient access for every running process. There are different scheduling algorithms used for different target purposes like operating systems (real-time or multi-threading systems), CPU architectures (x86 or embedded) or kernels (Linux or BSD).

In most cases, a scheduling algorithm is implemented as a part of the multitasking operating system responsible for determining the order of the processes to be executed. Apart from operating systems, it is also widely used in networks, databases and disk I/O. \\ \\


\section{Description of algorithms}

\subsection{First in first served}

The simplest scheduling algorithm to understand and get to know. FiFs makes a list of processes and the first process added to the queue is the first to be executed. Scheduler will allow to execute this process until it is finished. After that, another process will be executed.


\subsection{Round robin}

One of the easiest scheduling algorithms to implement and one of the easiest to understand. This algorithm is used in operating systems and gives every process the same execution time without taking any priorities into account.
Because of the fact that every process has the same priority, it is mostly used in systems where there are no priorities.

Round robin gives each process the same, fixed amount of the execution time. Let us assume that one process needs 102 ms to be finished, but allowed execution time is set to 100 ms. After 100 ms the process will be paused and added at the end of the queue and the next process will be executed for the next 100 ms.


\subsection{Shortest remaining time}

This scheduling algorithm takes into account the total approximate execution time of each process. The process with the total shortest time will be executed first. It is hard to predict which process will need the most time and which one the least time. So, in my experiment I assumed that the process with the shortest time is a process which needs the smallest number of cycles to do in the future. The list of jobs must be re-sorted after every cycle, because the job currently being executed has a shorter remaining time than it had the cycle before and new jobs could have been added.


\subsection{Shortest job first}

This method of scheduling will firstly execute the method with the shortest estimated time. Every time a new job is added to a list of jobs, the list must be re-sorted. This might cause a \textit{starvation problem}\cite{www:starvation}. The problem occurs when one very long job is waiting and still new shorter jobs are being added to the queue. The longest job might wait for a very long time until it will be executed.
\newpage

\subsection{Priority based scheduling}

This scheduling method is used in systems where a process has its own priority (most nowadays systems). The algorithm is very easy; the higher the priority the earlier a process will be executed. However, this scheduling algorithm has a pitfall, ie. different operating system use different measures for priorities. For example, in Linux kernel -20 is the highest priority and 19 is the lowest\cite{www:linux}, in Windows NT, 0 is the lowest and 30 is the highest\cite{www:windows}.

\section{Implementation}

\subsection{First in first served}
Implementation was provided by Richard Shipman. No changes were made.

\subsection{Round robin}
Implementation of round robin required to add two more lines in comparison to first-in first-served. \\
Changes included addition and removal of the same job from the queue in \textit{returnJob} method, so every time a job is not fully executed, it will go back at the end of the queue.
\begin{center}
\includegraphics[scale=0.5]{fc-rr}
\end{center}

\subsection{Shortest job first}
Implementation of this algorithm required me to modify \textit{addNewJob} method. Every time a new job is added to the list of jobs, the list is re-sorted. The order in the list is based on total estimated time of execution for each job.
\begin{center}
\includegraphics[scale=0.40]{fc-sjf}
\end{center}

\subsection{Shortest remaining time}
Implementation of SRT required me to write a simple sorting algorithm in \textit{getNextJob} method. Every time a new job is taken from the list of jobs, the list is resorted to return a process with the shortest total remaining time. 
\begin{center}
\includegraphics[scale=0.5]{fc-srt}
\end{center}

\subsection{Priority based scheduling}
In this algorithm's implementation I had to add a sorting loop to sort the list of jobs every time a new job is added. Function \textit{addNewJob} was modified. I am developing on Linux so I assumed that a job with lower priority number will be executed before a job with higher number.
\begin{center}
\includegraphics[scale=0.5]{fc-pq}
\end{center} \newpage

\section{Results}

\subsection{Approach}
To produce results, I made 4 tests for each of the aforementioned algorithms. All used test files were provided by Richard Shipman. A test file has a list of jobs with their names, priorities, start times, cycles on CPU and cycles blocked. \\
The tests were performed on a simulator provided also by Richard Shipman.
The simulator is able to save details about behaviour of any scheduling algorithm like:
\textit{mean elapsed duration, total CPU time, total context switches and total idle time}. \\
In my approach, I compared all 4 times for each algorithm, presented them as a column chart and drew conclusions.
The last conclusion I drew is presented in form of a general benchmarks table of all algorithms in best and worst cases. Numbers in bold are the best results in the test and numbers in italics are the worst. \\
In the following tables you can see a few letters labels; those are the acronyms of the names of the scheduling algorithms, thus
\textbf{RR} corresponds to \textbf{R}ound\textbf{R}obin, \textbf{SJF} to \textbf{S}hortest\textbf{J}ob\textbf{F}irst, etc.

\subsection{Conclusions}
Round robin could not do much in this case. This scheduler does not use any kind of sorting algorithms, each job is finished after some time. This algorithm has the best result in individual time, but because of stopping of the job and continuing it later, it has also the worst case in total time and in mean time. ShortestRemainingTime prioritizes processes with shortest time, so in this case SRT usually is the best or one of best in \textit{total time}.
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  & \multicolumn{5}{|c|}{Testing with \textit{Test.jobs}} \\
  \hline
  Algorithm & RR & SRT & SJF & FIFS & PQ\\ 
  \hline
  Individual time & \textbf{69} & 81 & 81 & 75 & \textit{85}\\ 
  Mean time & \textit{60} & \textbf{43} & 44 & 48 & 48\\ 
  Total time & \textit{301} & \textbf{219} & 221 & 244 & 242\\ \hline
  \end{tabular}
\end{table}
\includegraphics[width=\linewidth]{test1}
\line(1,0){240}

The second test shows us that in some cases RR and FIFS are exactly the same. This happens when the jobs are not long enough to break process' execution. SRT and SJF have the same times in individual time and mean time. Again SRT is the best in \textit{total time}, slightly better than SJF. Second time RR has the worst time in \textit{total time} rank.
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  & \multicolumn{5}{|c|}{Testing with \textit{Test2.jobs}} \\
  \hline
  Algorithm &  RR & SRT & SJF & FIFS & PQ\\ 
  \hline
  Individual time & \textbf{69} & \textit{82} & \textit{82} & \textbf{69} & 70\\ 
  Mean time &  \textit{59}  & \textbf{44} & \textbf{44} & 54 & 45\\ 
  Total time & \textit{296} & \textbf{222} & 224 & 273 & 229\\ \hline
  \end{tabular}
\end{table}
\includegraphics[width=\linewidth]{test2}
\line(1,0){240} \\ \\ \\

The third test was the longest provided test.
Again, this shows us huge differences between SRT with SJF to RR. Where RR, SRT and SJF have the same times in \textit{individual time}, but SRT and SJF are best in \textit{mean time} and \textit{total time}. This time average efficiency in \textit{total time} has PriorityQueue and FIFS is the worst one.
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  & \multicolumn{5}{|c|}{Testing with \textit{Test3.jobs}} \\
  \hline
  Algorithm &  RR & SRT & SJF & FIFS & PQ\\
  \hline
  Individual time & \textbf{182} & \textbf{182} & \textbf{182} & 191 & \textit{201}\\
  Mean time &  \textit{133} & \textbf{90} & \textbf{90} & 134 & 104\\
  Total time & 1197 & \textbf{817} & \textbf{817} & \textit{1213} & 937\\ \hline
  \end{tabular}
\end{table}
\includegraphics[width=\linewidth]{test3}
\line(1,0){240}

This test has the smallest number of cycles in total. Only three jobs and each of them has different priority. The diversity of priorities positively affects on PQ. Three jobs with three different priorities, so FIFS and PQ have exactly the same results, because the queue was very similiar.
\begin{table}[H]
\centering
  \begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  & \multicolumn{5}{|c|}{Testing with \textit{a.jobs}} \\
  \hline
  Algorithm & RR & SRT & SJF & FIFS & PQ\\
  \hline
  Individual time & \textbf{36} & \textit{52} & \textit{52} & 40 & 40\\
  Mean time	& \textit{31} & 29 & 30 & \textbf{27} & \textbf{27}\\
  Total time &	\textit{94} & 88 & 91 & \textbf{82} & \textbf{82}\\ \hline
  \end{tabular}
\end{table}
\includegraphics[width=\linewidth]{ajobs}


\section{Final conclusion}
In this section I will present to you the general conclusions and benchmarks. In comparsion of the algorithms the following four variables were used:  \textit{mean elapsed duration, total CPU time, total context switches} and total \textit{idle time}. These four values are printed to standard output while testing any of the algorithms. \\
In some cases, more than one algorithm had returned the best result, so every one of them was treated as the best one.

\subsection{The best algorithm}
This table shows us how many times the selected algorithm was the best in \textbf{all parts of testing}.
\begin{table}[H]
\centering
  \begin{tabular}{|c|c|}
  \hline
   Algorithm & Best times\\ \hline
   RR & 6 \\ \hline
   FIFS & 6 \\ \hline
   SRT & 5\\ \hline
   SJF & 4 \\ \hline
   PQ & 4 \\ \hline
  \end{tabular}
\end{table}

\subsection{The worst algorithms}
This table shows us how many times the selected algorithm was the worst in all parts of testing.
\begin{table}[H]
\centering
  \begin{tabular}{|c|c|}
  \hline
   Algorithm & Worst times\\ \hline
   RR & 7\\ \hline
   SRT & 4\\ \hline
   SJF & 4\\ \hline
   PQ & 4\\ \hline
   FIFS & 1\\ \hline
  \end{tabular}
\end{table}

\subsection{Conclusion}
It appeared that every of the algorithms was best and worst at least once. \\
We may assume that SJF and PQ are very average algorithms; their sorting is based on random variables, which cannot be predicted in real time environment; those variables depend on user's activity and OS's kernel. \\
What is very interesting is the fact that RR is the first one on the list of worst and best algorithms. This is because individual time is usually very low, what gives some "\textit{best}" points to this algorithm, but it also gets some "\textit{worst}" when we look at the total time. So, I think this algorithm is the worst one. \\
The following table shows us how negative points will affect positive points. However, the following table does not show that FIFS is the best scheduling algorithm. I think it is very medium; it can be quite efficient, but in very specified cases, eg. where are no priorities.
\begin{table}[H]
\centering
  \begin{tabular}{|c|c|}
  \hline
   Algorithm & Average times\\ \hline
   RR & 6-7 = -1\\ \hline
   SRT & 5-4 = 1\\ \hline
   SJF & 4-4 = 0\\ \hline
   PQ & 4-4 = 0\\ \hline
   FIFS & 6-1 = 5\\ \hline
  \end{tabular}
\end{table}

\bibliographystyle{abbrv}
\bibliography{sigproc} 

\balancecolumns
\end{document}