package uk.ac.aber.arp12.cs211.schedulersim.scheduler;

import java.util.*;
import uk.ac.aber.rcs.cs211.schedulersim.*;
import uk.ac.aber.rcs.cs211.schedulersim.scheduler.SchedulerException;

public class ShortestJobFirst implements Scheduler {

    protected ArrayList<Job> queue;
    private int numberOfJobs;

    public ShortestJobFirst() {
        this.queue = new ArrayList<>();
        this.numberOfJobs = 0;
    }

    @Override
    public void addNewJob(Job job) throws SchedulerException {

        if (queue.contains(job)) {
            throw new SchedulerException("Job already on Queue");
        }
        // add new job to the list
        // if the list is empty
        // and stop execution, quit method
        if (queue.isEmpty()) {
            queue.add(job);
            numberOfJobs++;
            return; //definitely stop execution!
        }

        for (int i = 0; i < queue.size(); i++) {
            
            if (job.getLength() < queue.get(i).getLength()) {
                queue.add(i, job); //adds new job in position i
                // the rest of the jobs are moved one place
                numberOfJobs++;
                return; //definitely stop execution!
            }
        } //for

        //and add anyway if was not stopped
        queue.add(job); //add new job at the end of the queue
        numberOfJobs++;

    } //addNewJob

    @Override
    public Job getNextJob() throws SchedulerException {

        Job lastJobReturned;

        if (this.numberOfJobs < 1) {
            throw new SchedulerException("Empty Queue");
        }

        lastJobReturned = (Job) this.queue.get(0);
        return lastJobReturned;
    }

    @Override
    public void returnJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }
    }

    @Override
    public void removeJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }

        this.queue.remove(job);
        this.numberOfJobs--;
    }

    @Override
    public void reset() {
        this.queue.clear();
        this.numberOfJobs = 0;
    }

    @Override
    public Job[] getJobList() {

        Job[] jobs = new Job[queue.size()];

        for (int i = 0; i < queue.size(); i++) {
            jobs[i] = this.queue.get(i);
        }

        return jobs;

    }
}
