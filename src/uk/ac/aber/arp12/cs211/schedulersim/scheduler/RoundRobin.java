package uk.ac.aber.arp12.cs211.schedulersim.scheduler;

import java.util.*;
import uk.ac.aber.rcs.cs211.schedulersim.*;
import uk.ac.aber.rcs.cs211.schedulersim.scheduler.SchedulerException;

public class RoundRobin implements Scheduler {

    protected ArrayList<Job> queue;
    private int numberOfJobs;

    public RoundRobin() {
        this.queue = new ArrayList<>();
        this.numberOfJobs = 0;
    }

    @Override
    public void addNewJob(Job job) throws SchedulerException {

        if (this.queue.contains(job)) {
            throw new SchedulerException("Job already on Queue");
        }

        this.queue.add(this.numberOfJobs, job);
        this.numberOfJobs++;
    }

    @Override
    public Job getNextJob() throws SchedulerException {
        Job lastJobReturned;
        if (this.numberOfJobs < 1) {
            throw new SchedulerException("Empty Queue");
        }
        lastJobReturned = (Job) this.queue.get(0);
        return lastJobReturned;
    }

    @Override
    public void returnJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }

        this.queue.remove(job); //removes current job from the queue
                               //after some execution time
        this.queue.add(job);  //adds current job at the end of the queue
    }

    @Override
    public void removeJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }

        this.queue.remove(job);
        this.numberOfJobs--;
    }

    @Override
    public void reset() {
        this.queue.clear();
        this.numberOfJobs = 0;
    }

    @Override
    public Job[] getJobList() {

        Job[] jobs = new Job[queue.size()];

        for (int i = 0; i < queue.size(); i++) {
            jobs[i] = this.queue.get(i);
        }

        return jobs;

    }
}
