package uk.ac.aber.arp12.cs211.schedulersim.scheduler;

import java.util.*;
import uk.ac.aber.rcs.cs211.schedulersim.*;
import uk.ac.aber.rcs.cs211.schedulersim.scheduler.SchedulerException;

public class ShortestRemainingTime implements Scheduler {

    protected ArrayList<Job> queue;
    private int numberOfJobs;

    public ShortestRemainingTime() {
        this.queue = new ArrayList<>();
        this.numberOfJobs = 0;
    }

    @Override
    public void addNewJob(Job job) throws SchedulerException {

        if (this.queue.contains(job)) {
            throw new SchedulerException("Job already on Queue");
        }

        this.queue.add(this.numberOfJobs, job);
        this.numberOfJobs++;
    }

    @Override
    public Job getNextJob() throws SchedulerException {

        if (this.numberOfJobs < 1) {
            throw new SchedulerException("Empty Queue");
        }

        Job lastJobReturned = queue.get(0); // take temporary job
        int shortestJobSoFar = 0; // temporary job is the shortest job so far...

        for (int i = 0; i < queue.size(); i++) {

            if (lastJobReturned.getLength() - lastJobReturned.getProgramCounter()
                    > queue.get(i).getLength() - queue.get(i).getProgramCounter()) {
                //we have new shortest job
                lastJobReturned = queue.get(i);

                // swap job in the list
                // so it looks nicer in GUI
                // not swapping doesnt change execution order
                Collections.swap(queue, i, shortestJobSoFar);
                //job number i is the shortest now
                shortestJobSoFar = i;
            }
        } //for

        return lastJobReturned;
    }

    @Override
    public void returnJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }

    } //returnJob

    @Override
    public void removeJob(Job job) throws SchedulerException {
        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }
        this.queue.remove(job);
        this.numberOfJobs--;
    }

    @Override
    public void reset() {
        this.queue.clear();
        this.numberOfJobs = 0;
    }

    @Override
    public Job[] getJobList() {
        Job[] jobs = new Job[queue.size()];
        for (int i = 0; i < queue.size(); i++) {
            jobs[i] = this.queue.get(i);
        }
        return jobs;
    }
}
