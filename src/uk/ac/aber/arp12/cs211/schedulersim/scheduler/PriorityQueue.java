package uk.ac.aber.arp12.cs211.schedulersim.scheduler;

import java.util.ArrayList;
import uk.ac.aber.rcs.cs211.schedulersim.Job;
import uk.ac.aber.rcs.cs211.schedulersim.Scheduler;
import uk.ac.aber.rcs.cs211.schedulersim.scheduler.SchedulerException;

public class PriorityQueue implements Scheduler {

    protected ArrayList<Job> queue;
    private int numberOfJobs;

    public PriorityQueue() {
        this.queue = new ArrayList<>();
        this.numberOfJobs = 0;
    }

    @Override
    public void addNewJob(Job job) throws SchedulerException {

        if (this.queue.contains(job)) {
            throw new SchedulerException("Job already on Queue");
        }

        // add new job to the list
        // if the list is empty
        // and stop execution, quit method
        if (queue.isEmpty()) {
            queue.add(job);
            numberOfJobs++;
            return; //definitely stop execution!
        }

        for (int i = 0; i < queue.size(); i++) {
            //compare priorities
            if (job.getPriority() < queue.get(i).getPriority()) {
                //if lower, place new job at i
                queue.add(i, job);
                //the rest of jobs in the queue are moved by one
                numberOfJobs++;
                return;  //definitely stop execution!
            } //if

        } //for each job in queue

        //add anyway if method was not stopped
        queue.add(job); //add new job at the end
        numberOfJobs++;

    } //addNewJob

    @Override
    public Job getNextJob() throws SchedulerException {

        Job lastJobReturned;
        if (this.numberOfJobs < 1) {
            throw new SchedulerException("Empty Queue");
        }

        lastJobReturned = (Job) this.queue.get(0);
        return lastJobReturned;
    }

    @Override
    public void returnJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }
    }

    @Override
    public void removeJob(Job job) throws SchedulerException {

        if (!this.queue.contains(job)) {
            throw new SchedulerException("Job not on Queue");
        }

        this.queue.remove(job);
        this.numberOfJobs--;
    }

    @Override
    public void reset() {
        this.queue.clear();
        this.numberOfJobs = 0;
    }

    @Override
    public Job[] getJobList() {

        Job[] jobs = new Job[queue.size()];

        for (int i = 0; i < queue.size(); i++) {
            jobs[i] = this.queue.get(i);
        }

        return jobs;
    }
}